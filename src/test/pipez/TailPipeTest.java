package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class TailPipeTest {

	@Test
	public void test_getName() throws Exception{
		TailPipe pipe = TailPipe.create("");
		
		assertThat(pipe.getName(), is("Tail debugger"));
		
	}
	
	@Test
	public void test_create() throws Exception {
		TailPipe pipe = TailPipe.create();
		
		assertThat(pipe.getName(), is("Tail debugger"));
		
	}
	@Test
	public void test_create_string() throws Exception {
		TailPipe pipe = TailPipe.create((System.out),"bye");
		
		assertThat(pipe.getName(), is("Tail debugger"));
		
	}
	@Test
	public void test_transform() throws Exception {
		TailPipe pipe = TailPipe.create("Colum");
		SimpleBlock sb = new SimpleBlock("a","b","c");
		Block b = pipe.transform(sb);
		
	}
	
}
