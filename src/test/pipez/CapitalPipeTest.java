package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class CapitalPipeTest {

	@Test
	public void test_getName(){
		CapitalPipe Cp = CapitalPipe.create();
		assertThat(Cp.getName(),is("CapitalPipe"));
	}
	@Test
	public void test_Capitalletter(){
		CapitalPipe Cp = CapitalPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "abc");
		Block tb = Cp.transform(sb);
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.value("C1"), is("ABC"));
	}
}
