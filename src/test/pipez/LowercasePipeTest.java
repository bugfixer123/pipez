package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class LowercasePipeTest {

	@Test
	public void test_getName(){
		LowercasePipe lp = LowercasePipe.create();
		assertThat(lp.getName(),is("toLowerCasePipe"));
	}
	@Test
	public void test_Capitalletter(){
		LowercasePipe lp = LowercasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC");
		Block tb = lp.transform(sb);
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.value("C1"), is("abc"));
	}
}