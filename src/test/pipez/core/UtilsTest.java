package pipez.core;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;

import org.junit.Test;
import pipez.core.*;
import pipez.io.CSVWriter;

import static org.hamcrest.CoreMatchers.is;

public class UtilsTest {

	@Test
	public void stringsOfIntTest() {
		String str[] = Utils.stringsOf(new int[]{1,2,3,4});
		for(int i = 1; i < 5; i++){
			assertThat(str[i-1],is(""+i));
		}
	}
	@Test
	public void stringsOfDoubleTest() {
		String str[] = Utils.stringsOf(new double[]{1.0,2.0,3.0,4.0});
		for(double i = 1.0; i < 5.0; i++){
			assertThat(str[(int)i-1],is(""+i));
		}
	}
	@Test
	public void intsOfTest() {
		int ints[] = Utils.intsOf(new String[]{"1","2","3","4"});
		for(int i = 1; i < 5; i++){
			assertThat(ints[i-1],is(i));
		}
	}
	
	@Test
	public void dblsOfTest() {
		double dbls[] = Utils.dblsOf(new String[]{"1.0","2.0","3.0","4.0"});
		for(double i = 1.0; i < 5.0; i++){
			assertThat(dbls[(int)i-1],is(i));
		}
	}
	
	@Test
	public void intsOf_exception_Test() {
	
		PrintStream err = mock(PrintStream.class);
		PrintStream orierr = System.err;
		System.setErr(err);
		
		Utils.intsOf(new String[]{"a","2","3","4"});
		
		verify(err).println(eq("a" + " could not be converted to an integer."));
		System.setErr(orierr);
	}
	
	@Test
	public void dblsOf_exception_Test() {
		PrintStream err = mock(PrintStream.class);
		PrintStream orierr = System.err;
		System.setErr(err);
		
		Utils.dblsOf(new String[]{"1.0","b","3.0","4.0"});
		
		verify(err).println(eq("b" + " could not be converted to a double."));
		System.setErr(orierr);
	}
}
