package pipez.core;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static pipez.util.TestUtils.are;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import pipez.EvenFieldsPipe;
import pipez.IdentityPipe;
import pipez.OddFieldsPipe;
import pipez.RenameFieldsPipe;
import pipez.ReversePipe;
import pipez.ValueMatchPipe;
import pipez.io.CSVReader;
import pipez.io.CSVWriter;
import pipez.io.Reader;
import pipez.io.Writer;


public class PipeLineTest {
	@Test 
	public void exceptionNoAppendingTest(){
		Pipeline pl =  Pipeline.create("test");
		
		PrintStream err = mock(PrintStream.class);
		PrintStream orierr = System.err;
		System.setErr(err);
		pl.out(CSVWriter.to("nothing"));
		
		verify(err).println(eq("Pipeline " + "test" + " is empty and cannot be executed."));
		System.setErr(orierr);
	}
	
	@Test
	public void exceptionHandlerNullwriterTest(){
		Pipeline pl =  Pipeline.create("test");
		
		PrintStream err = mock(PrintStream.class);
		PrintStream orierr = System.err;
		System.setErr(err);
		pl.in(CSVReader.from("in/in.csv"))
		
		.append(IdentityPipe.create())
		.out(null);
		
		verify(err).println(eq("Pipeline " + "test" + " out() cannot be provided with a null Writer argument."));
		System.setErr(orierr);
	}

	@Test
	public void c6_reverse_odd_test() {
		Pipeline.create("6column, reverse, odd")
		
			.in(CSVReader.from("in/6c_pipelinetest.csv"))
			
			.append(IdentityPipe.create())
			.append(ReversePipe.create())
			.append(OddFieldsPipe.create())
			
			.out(CSVWriter.to("out/6c_pipelinetest.csv"));
		
		Reader csv = CSVReader.from("out/6c_pipelinetest.csv");
		Block b = csv.next();
		assertThat(b.fields().length,is(3));
		assertThat(b.values(),are("V6", "V4", "V2"));
		b = csv.next();
		assertThat(b.values(),are("6", "4", "2"));
		assertThat(b.fields().length,is(3));
		csv.close();
	}
	
	@Test
	public void count_test() {
		Pipeline.createCountline("6line count")
		
			.in(CSVReader.from("in/in.csv"))
			
			.append(IdentityPipe.create())
			
			.out(CSVWriter.to("out/out.csv"));
		
		Reader csv = CSVReader.from("out/out.csv");
		Block b = csv.next();
		assertThat(b.fields().length,is(6));
		assertThat(b.values()[0],is("Count"));
		b = csv.next();
		assertThat(b.values(),are("6"));
		assertThat(b.fields().length,is(1));
		csv.close();
	}
	
	@Test
	public void selectWhere_test() {
		Pipeline.create("Select all Mr")
		
			.in(CSVReader.from("in/dataSet.csv"))
			
			.append(IdentityPipe.create())
			.selectWhere("V2", "Mr")
			.out(CSVWriter.to("out/selectDataSet.csv"));
		
		Reader csv = CSVReader.from("out/selectDataSet.csv");
		int lineCounter = 0;
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			assertThat(b.fields().length, is(6));
			assertThat(b.value(b.fields()[1]), is("Mr"));
			lineCounter++;
		}
		assertThat(lineCounter, is(6));

		csv.close();
	}
	
	@Test
	public void order_alpha_ASC_test() {
		Pipeline.create("mix count and select")
		
			.in(CSVReader.from("in/dataSet.csv"))
			
			.append(RenameFieldsPipe.create(new String[]{"V1","V2","V3","V4","V5","V6"}
										, new String[]{"member_id","title","name","age","gender","phone#"}))
			.order("name", "ASC")
			.out(CSVWriter.to("out/orderDataSet.csv"));
		
		Reader csv = CSVReader.from("out/orderDataSet.csv");
		int lineCounter = 0;
		ArrayList<String> values = new ArrayList<String>();
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			values.add(b.value("V3"));
			assertThat(b.fields().length, is(6));
 			lineCounter++;
		}
		assertThat(lineCounter, is(10));
		boolean ordered = true;
		for(int i = 0; i < lineCounter - 1; i++){
			if(values.get(i).compareTo(values.get(i+1)) > 0)
				ordered = false;
		}
		assertThat(ordered, is(true));
		csv.close();
	}
	
	@Test
	public void order_alpha_DESC_test() {
		Pipeline.create("mix count and select")
		
			.in(CSVReader.from("in/dataSet.csv"))
			
			.append(RenameFieldsPipe.create(new String[]{"V1","V2","V3","V4","V5","V6"}
										, new String[]{"member_id","title","name","age","gender","phone#"}))
			.order("name", "DESC")
			.out(CSVWriter.to("out/orderDataSet.csv"));
		
		Reader csv = CSVReader.from("out/orderDataSet.csv");
		int lineCounter = 0;
		ArrayList<String> values = new ArrayList<String>();
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			values.add(b.value("V3"));
			assertThat(b.fields().length, is(6));
 			lineCounter++;
		}
		assertThat(lineCounter, is(10));
		boolean ordered = true;
		for(int i = 0; i < lineCounter - 1; i++){
			if(values.get(i).compareTo(values.get(i+1)) < 0)
				ordered = false;
		}
		assertThat(ordered, is(true));
		csv.close();
	}
	
	@Test
	public void order_numeric_ASC_test() {
		Pipeline.create("mix count and select")
		
			.in(CSVReader.from("in/dataSet.csv"))
			
			.append(RenameFieldsPipe.create(new String[]{"V1","V2","V3","V4","V5","V6"}
										, new String[]{"member_id","title","name","age","gender","phone#"}))
			.order("member_id", "ASC")
			.out(CSVWriter.to("out/orderDataSet.csv"));
		
		Reader csv = CSVReader.from("out/orderDataSet.csv");
		int lineCounter = 0;
		ArrayList<Integer> values = new ArrayList<Integer>();
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			values.add(Integer.parseInt(b.value("V1")));
			assertThat(b.fields().length, is(6));
 			lineCounter++;
		}
		assertThat(lineCounter, is(10));
		boolean ordered = true;
		for(int i = 0; i < lineCounter - 1; i++){
			if(values.get(i) - values.get(i+1) > 0)
				ordered = false;
		}
		assertThat(ordered, is(true));
		csv.close();
	}
	
	@Test
	public void order_null_DESC_handle() {
		Writer w = CSVWriter.to("in/nullData.csv");
		for(int i=1; i<11; i++) {
			SimpleBlock sb = new SimpleBlock();
			if(i%4 == 0)
				sb.add("V1",null);
			else if(i%3 == 0)
				sb.add("V1","a");
			else
				sb.add("V1","b");
			w.write(sb);
		}
		w.close();
		//for DESC
		Pipeline.create("mix count and select")
			
			.in(CSVReader.from("in/nullData.csv"))
			
			.order("V1", "DESC")
			.out(CSVWriter.to("out/nullData.csv"));
		
		Reader csv = CSVReader.from("out/nullData.csv");
		
		int lineCounter = 0;
		ArrayList<String> values = new ArrayList<String>();
		if(csv.hasNext())csv.next();
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			values.add(b.value("V1"));
				lineCounter++;
		}
		assertThat(lineCounter, is(10));
		boolean ordered = true;
		for(int i = 0; i < 6; i++){
			if(values.get(i).compareTo(values.get(i+1)) < 0)
				ordered = false;
		}
		for(int i = 8; i < 10; i++){
			if(values.get(i) != null)
				ordered = false;
		}
		assertThat(ordered, is(true));
		csv.close();
	}	
	
	@Test
	public void  order_null_ASC_handle(){
		Writer w = CSVWriter.to("in/nullData.csv");
		for(int i=1; i<11; i++) {
			SimpleBlock sb = new SimpleBlock();
			if(i%4 == 0)
				sb.add("V1",null);
			else if(i%3 == 0)
				sb.add("V1","a");
			else
				sb.add("V1","b");
			w.write(sb);
		}
		w.close();
		
		Pipeline.create("mix count and select")
			
			.in(CSVReader.from("in/nullData.csv"))
			.append(ValueMatchPipe.createInverse("V1"))
			.order("V1", "ASC")
			.out(CSVWriter.to("out/nullData.csv"));
	
		CSVReader csv = CSVReader.from("out/nullData.csv");
		
		int lineCounter = 0;
		ArrayList<String>values = new ArrayList<String>();
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			values.add(b.value("V1"));
				lineCounter++;
		}
		assertThat(lineCounter, is(10));
		boolean ordered = true;
		for(int i = 2; i < lineCounter - 1; i++){
			if(values.get(i).compareTo(values.get(i+1)) > 0)
				ordered = false;
		}
		assertThat(ordered, is(true));
		
		for(int i = 0; i < 2; i++){
			if(values.get(i) != null)
				ordered = false;
		}
		assertThat(ordered, is(true));
		csv.close();
	}
	
	@Test
	public void mixed_test() {
		Pipeline.createCountline("mix count and select")
		
			.in(CSVReader.from("in/dataSet.csv"))
			
			.append(RenameFieldsPipe.create(new String[]{"V1","V2","V3","V4","V5","V6"}
										, new String[]{"member_id","title","name","age","gender","phone#"}))
			.selectWhere("gender", "male")
			.order("member_id", "DESC")
			.out(CSVWriter.to("out/mixDataSet.csv"));
		
		Reader csv = CSVReader.from("out/mixDataSet.csv");
		int lineCounter = 0;
		ArrayList<Integer> values = new ArrayList<Integer>();
		if(csv.hasNext())csv.next();
		if(csv.hasNext())csv.next();
		while(csv.hasNext()){
			Block b = csv.next();
			values.add(Integer.parseInt(b.value("V2")));
			assertThat(b.fields().length, is(7));
 			lineCounter++;
		}
		assertThat(lineCounter, is(6));
		boolean ordered = true;
		for(int i = 0; i < lineCounter - 1; i++){
			if(values.get(i) - values.get(i+1) < 0)
				ordered = false;
		}
		assertThat(ordered, is(true));
		csv.close();
	}
}
