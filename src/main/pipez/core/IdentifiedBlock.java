package pipez.core;

import java.util.Comparator;
 
public class IdentifiedBlock{
 
    private String idField;
    
    private Block block;
 
    public IdentifiedBlock(Block block, String idField) {
    	this.idField = idField;
    	this.block = block;
    }
    
    public Block getBlock(){
    	return this.block;
    }
    
    public String getField(){
    	return this.idField;
    }
}
 
class ASCComparator implements Comparator<IdentifiedBlock> {
 
    public int compare(IdentifiedBlock first, IdentifiedBlock second) {
    	String firstStr = first.getBlock().value(first.getField());
    	String secondStr = second.getBlock().value(second.getField());
    	if(firstStr == null && secondStr == null) return 0;
    	if(firstStr == null) return -1;
    	if(secondStr == null) return 1;
    	try{
        	Double firstD = Double.parseDouble(firstStr);
        	Double secondD = Double.parseDouble(secondStr);
            return (int)(firstD - secondD);
    	}catch(NumberFormatException e){
    			return firstStr.compareToIgnoreCase(secondStr);
    	}
    } 
}
 
class DESCComparator implements  Comparator<IdentifiedBlock> {
 
    public int compare(IdentifiedBlock first, IdentifiedBlock second) {
    	String firstStr = first.getBlock().value(first.getField());
    	String secondStr = second.getBlock().value(second.getField());
    	if(firstStr == null && secondStr == null) return 0;
    	if(firstStr == null) return 1;
    	if(secondStr == null) return -1;
    	try{
    		Double firstD = Double.parseDouble(firstStr);
    		Double secondD = Double.parseDouble(secondStr);
    		return (int)(secondD - firstD);
		}catch(NumberFormatException e){
			return secondStr.compareToIgnoreCase(firstStr);
		}
    }
}