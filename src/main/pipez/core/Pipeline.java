package pipez.core;


import java.util.ArrayList;

import pipez.io.Reader;
import pipez.io.Writer;

public class Pipeline {

	private String name;
	private Reader reader;
	private Writer writer;
	private Piper head, tail; //the head and tail of this pipeline; 
	//the head is the source of initial data or connects to the pipeline's datasource
	// and the tail produces the final output of the pipeline
	private int blockCounter;
	private boolean whetherCount = false;
	/**
	 * Piper is an internal wrapper around Pipe for use by this engine.
	 * 
	 * @author whwong
	 *
	 */
	private class Piper{
		
		private class EOSException extends Exception{}; //exception thrown when no more data 
		
		Reader reader;
		Piper pred;
		Pipe pipe;
		
		Piper(Reader read){
			if(read == null) throw new IllegalArgumentException("Reader cannot be null");
//			if(read == null) PipezExceptionHandler.handle(new IllegalArgumentException("Reader cannot be null"));
			this.reader = read;
		}
		
		Piper(Piper pred, Pipe pipe){
			//if(pred == null) throw new IllegalArgumentException("Predecessor cannot be null");
//			if(pipe == null) PipezExceptionHandler.handle(new IllegalArgumentException("Pipe cannot be null"));
			if(pipe == null) throw new IllegalArgumentException("Pipe cannot be null");
			this.pred = pred;
			this.pipe = pipe;
		}
		
		public Block invoke() throws EOSException{
			
			if(reader != null) {
				if(!reader.hasNext()) throw new EOSException();
				return reader.next();
			}
			
			return pipe.transform(pred.invoke());
		}
	}
	
	public static Pipeline create(String name) {
		return new Pipeline(name,false);
	}
	
	public static Pipeline createCountline(String name) {
		return new Pipeline(name,true);
	}
	
	private Pipeline(String name, boolean whetherCount) {
		this.whetherCount = whetherCount;
		this.whetherMatch = false;
		this.whetherOrder = false;
		this.name = name;
	}
	
	private String fieldToMatch;
	private String valueToMatch;
	private boolean whetherMatch;
	
	public Pipeline selectWhere(String field, String value){
		this.fieldToMatch = field;
		this.valueToMatch = value;
		this.whetherMatch = true;
		return this;
	}
	
	private String fieldToOrder;
	private String way;
	private boolean whetherOrder;
	
	public Pipeline order(String fieldToOrder, String way){
		this.fieldToOrder = fieldToOrder;
		this.way = way;
		this.whetherOrder = true;
		return this;
	}
//	public Pipeline merge(Pipeline a, Pipeline b) {
//		
//		return this;
//	}
//	
//	public Pipeline[] tee() {
//		
//	}
	
	public Pipeline in(Reader reader) {
		this.reader = reader;
		Piper p = new Piper(reader);
		if(head != null) {
			head.pred = p;
		}
		head = p;
		if(tail == null) tail = p;
		return this;
	}
	
	public Pipeline append(Pipe pipe) {
		Piper p = new Piper(tail, pipe);
		if(head == null) head = p;
		tail = p;
		return this;
	}

	public void out(Writer writer) {
//		if(reader == null) throw new Exception(); //a pipelie can start without a reader e.g. merge of two pipelines
		if(reader != null) reader.open();
		if(head == null || tail == null) {
			PipezExceptionHandler.handle(
					new Exception("Pipeline " + name + " is empty and cannot be executed."));
			return;
		}
		
		if(writer == null) {
			PipezExceptionHandler.handle(
					new Exception("Pipeline " + name + " out() cannot be provided with a null Writer argument."));
			return;
		}
		
		this.writer = writer;
		// prepare writer
		writer.open();
		ArrayList<IdentifiedBlock> blocks = new ArrayList<IdentifiedBlock>();
		// this will run the whole pipeline
		// review for merge() especially EOSException
		try {
			blockCounter = 0;
			while(true) {
				IdentifiedBlock bl = new IdentifiedBlock(tail.invoke(),fieldToOrder);
				if(whetherMatch == true){
					if(bl.getBlock().value(fieldToMatch) != null) {
						if(bl.getBlock().value(fieldToMatch).equals(valueToMatch)){
							blocks.add(bl);
							blockCounter++;
						}
					}
				}else{
					blocks.add(bl);
					blockCounter++;
				}
				 // this will run the entire pipeline for a single block
				//if(blocks[blockCounter] != SpecialBlocks.SKIP_BLOCK)  writer.write(block); //skip this block
			}
		}catch(Piper.EOSException eos) {
			// end of data 
		}finally {
			if(reader != null) reader.close();
			if(whetherOrder){
				if(way.equals("DESC"))
					blocks.sort(new DESCComparator());
				else
					blocks.sort(new ASCComparator());
			}
			if(whetherCount == true){
				SimpleBlock block = new SimpleBlock();
				block.add("Count",Integer.toString(blockCounter));
				writer.write(block); 
			}
			for(int i = 0; i< blockCounter; i++){
				if(blocks.get(i).getBlock() != SpecialBlocks.SKIP_BLOCK)  writer.write(blocks.get(i).getBlock());
			}

			writer.close();
		}
		
	}
}
