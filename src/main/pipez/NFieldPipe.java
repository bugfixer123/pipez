package pipez;

import java.util.Arrays;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.PipezExceptionHandler;
import pipez.core.SimpleBlock;

/**
 * Selects the n-th field of each block
 * e.g. select the n-th column of a CSV file.
 * 
 *  n=1 corresponds to the first column.
 *  n=-1 corresponds to the last column.
 *  
 * Also allows multiple fields to be selected.
 * 
 * @author whwong
 *
 */
public class NFieldPipe implements Pipe {

	public static NFieldPipe create(int n) {
		return new NFieldPipe(n);
	}

	public static NFieldPipe create(int... ns) {
		return new NFieldPipe(ns);
	}
	
	int[] ns;
	private NFieldPipe(int... ns) {
		this.ns = ns;
		int i=0;
		for(int n :ns) ns[i++] = n - 1;
	}
	
	
	@Override
	public String getName() {
		return "n-th field pipe";
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock newBlock = new SimpleBlock();
	
		String[] fields = block.fields();
		String[] values = block.values();
		for(int n: ns) {
			if(Math.abs(n) >fields.length){
				continue;
			}else {
				if(n>0){
					newBlock.add(fields[n] , values[n]);
				}else if(n==0){
					newBlock.add(fields[n] , values[n]);
				}else if(n<0){
					n += fields.length;
					n = n+1;
					newBlock.add(fields[n] , values[n]);
				
				}
			}
		}
		
		return newBlock;
	}

}
