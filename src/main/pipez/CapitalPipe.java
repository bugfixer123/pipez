package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

public class CapitalPipe implements Pipe {

	@Override
	public String getName() {
		return "CapitalPipe";
		
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock newBlock = new SimpleBlock();
		String[] values = block.values();
		String[] fields = block.fields();
		for(int i =0 ;i<values.length;i++){
			 newBlock.add(fields[i], values[i].toUpperCase());
			} return newBlock;
		}

	public static CapitalPipe create() {
		return new CapitalPipe();
	}
	}


